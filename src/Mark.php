<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/22/2017
 * Time: 7:08 PM
 */

namespace App;

class Mark
{
    protected $procesor;
    protected $periperals;
    protected $system;
    protected $network;

    public function setdata($postarray)
    {
        if(array_key_exists("pro", $postarray))
        {
            $this->procesor=$postarray['pro'];
        }
        if(array_key_exists("net", $postarray))
        {
            $this->periperals=$postarray['net'];
        }
        if(array_key_exists("sys", $postarray))
        {
            $this->system=$postarray['sys'];
        }
        if(array_key_exists("per", $postarray))
        {
            $this->network=$postarray['per'];
        }
    }
    public function getdata()
    {
        $procesor=$this->procesor;
        $periperals=$this->periperals;
        $system=$this->system;
        $network=$this->network;
        $list=array("procesor","periperals","system","network");
        $singleinfo=compact($list);
        return $singleinfo;
    }
}