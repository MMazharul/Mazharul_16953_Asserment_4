<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 5/22/2017
 * Time: 7:08 PM
 */

namespace App;


class Student
{
    protected $username;
    protected $id;
    public function setdata($postarray)
    {
        if(array_key_exists("u",$postarray))
        {
            $this->username=$postarray['u'];
        }

        if(array_key_exists("i",$postarray))
        {
            $this->id=$postarray['i'];
        }
    }
    public function getdata()
    {
        $username=$this->username;
        $id=$this->id;
        $list=array("username","id");
        $studentinfo=compact($list);
        return $studentinfo;

    }

}